EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR0101
U 1 1 5F81F008
P 6050 3750
F 0 "#PWR0101" H 6050 3500 50  0001 C CNN
F 1 "GND" H 6055 3577 50  0000 C CNN
F 2 "" H 6050 3750 50  0001 C CNN
F 3 "" H 6050 3750 50  0001 C CNN
	1    6050 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C1
U 1 1 5F81E654
P 6050 3600
F 0 "C1" H 6168 3646 50  0000 L CNN
F 1 "CP" H 6168 3555 50  0000 L CNN
F 2 "test2:CP_Radial_D10.0mm_P2.5_3.5_5.0" H 6088 3450 50  0001 C CNN
F 3 "~" H 6050 3600 50  0001 C CNN
	1    6050 3600
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0102
U 1 1 5F81F536
P 6050 3450
F 0 "#PWR0102" H 6050 3300 50  0001 C CNN
F 1 "+5V" H 6065 3623 50  0000 C CNN
F 2 "" H 6050 3450 50  0001 C CNN
F 3 "" H 6050 3450 50  0001 C CNN
	1    6050 3450
	1    0    0    -1  
$EndComp
$EndSCHEMATC
